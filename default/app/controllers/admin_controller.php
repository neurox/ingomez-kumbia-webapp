<?php

class AdminController extends AppController
{

  protected function before_filter()
  {
    View::template('main');

    if(!Auth::is_valid()){
      Redirect::to('usuarios/login');
    }

    $this->username = Auth::get('fname');

  }

  public function index()
  {
    Redirect::toAction('panel');
  }

  public function panel()
  {
    $this->pagina = 'adminpanel';
  }

}

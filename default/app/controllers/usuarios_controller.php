<?php

Load::lib('auth');

Load::model('usuarios');

class UsuariosController extends AppController
{

  protected function before_filter()
  {
    View::template('main');

    $this->username = '';
    $this->pagina = '';
  }

  public function register()
  {

    if(!Auth::is_valid()){
      Flash::error("Esa función esta en desarrollo...");
      Redirect::to('usuarios/login');
    }

    if(Input::hasPost(
      "username",
      "password",
      "email",
      "fname",
      "lname"
    )) {

      $username = Input::post("username");
      $password = Input::post("password");
      $email = Input::post("email");
      $fname = Input::post("fname");
      $lname = Input::post("lname");

      $salt = substr(base64_encode(openssl_random_pseudo_bytes('30')), 0, 22);
      $salt = strtr($salt, array('+' => '.'));
      $hash = crypt($password, '$2y$10$' . $salt);

      $Usuario = new Usuarios();
      $Usuario->username = $username;
      $Usuario->password = $hash;
      $Usuario->email = $email;
      $Usuario->fname = $fname;
      $Usuario->lname = $lname;
      $Usuario->save();

    }
  }

  public function login()
  {

    $Usuarios = new Usuarios();

    if (Input::hasPost("username","password")){

        $user = Input::post("username");
        $pass = Input::post("password");

        $user = htmlspecialchars($user, ENT_QUOTES, 'UTF-8');
        $pass = htmlspecialchars($pass, ENT_QUOTES, 'UTF-8');

        // Buscamos el hash con el nombre de usuario
        foreach($Usuarios->find("conditions: username='$user'") as $Usuario){
               $dbHash = $Usuario->password;
        }

        $pass = crypt($pass, $dbHash);

        $auth = new Auth("model", "class: usuarios", "username: $user", "password: $pass");
        if ($auth->authenticate()) {
          Redirect::to("admin/panel");
        } else {
            Flash::error("Falló");
        }

        $this->username = Auth::get('fname');
    }
  }

  public function logout()
  {
    Auth::destroy_identity();
  }

}

<?php

class SeccionController extends AppController
{

  protected function before_filter()
  {
    View::template('main');

    $this->username = Auth::get('fname');
  }

  public function index()
  {
    Redirect::toAction('inicio');
  }

  public function inicio()
  {
    $this->pagina = 'inicio';
  }

  public function acerca()
  {
    function edad($fecha){
        $fecha = str_replace("/","-",$fecha);
        $fecha = date('Y/m/d',strtotime($fecha));
        $hoy = date('Y/m/d');
        $edad = $hoy - $fecha;
        return $edad;
    }

    $this->myAge = edad('1986/02/03');
    $this->pagina = 'acerca';
  }

  public function servicios()
  {
    $this->pagina = 'servicios';
  }

  public function contacto()
  {
    $this->pagina = 'contacto';
  }

}

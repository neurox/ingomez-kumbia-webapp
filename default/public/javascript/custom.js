const HOST = location.protocol+'//'+location.host+'/';

$(function(){

  $(".button-collapse").sideNav();

  $('.collapsible').collapsible();

  $('#accept-button').on({'click': function(evt){
    evt.preventDefault();
  }});

  $('#whatsapp-modal-button').on({'click': function(evt){
    evt.preventDefault();
    $('#whatsapp-modal').modal('open');
  }});

});

$(document).ready(function() {

    $('#whatsapp-modal').modal();

});

$('#logout').on({'click':function(evt){
  evt.preventDefault();

  $.ajax({
    type: "POST",
    url: HOST+"usuarios/logout"
  }).always(function(data) {
    location = HOST;
  });

}});
